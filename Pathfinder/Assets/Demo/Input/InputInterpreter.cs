﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputInterpreter : MonoBehaviour
{
    [Header("Scripts")]
    [SerializeField] private InputListener _InputListener = null;
    [SerializeField] private GridController _PathGridController = null;
    [SerializeField] private ScreenController _ScreenController = null;

    private void Awake() 
    {
        _InputListener.movementChanged += OnControlChanged;
        _InputListener.moveableCellChanged += OnMoveableCellChanged;
    }

    private void OnMoveableCellChanged(CellState state)
    {
        switch(state)
        {
            case CellState.START:
            _ScreenController.ChangeMoveableCellLabel(state);
            break;
            case CellState.FINNISH:
            _ScreenController.ChangeMoveableCellLabel(state);
            break;
            default: throw new NotImplementedException();
        }
    }

    private void OnControlChanged(MovementVector vector)
    {
        
        if(vector != MovementVector.NONE)
        {
            Vector2Int? currentPos = null;
            Vector2Int? desiredPos = null;
            positionalDelegate del = null;
            
            /* getting current position and delegate of the right function for position change */
            switch(_InputListener.MoveableCell)
            {
                case CellState.START: 
                currentPos = _PathGridController.StartPosition;
                del = _PathGridController.SetStart;
                break;
                case CellState.FINNISH: 
                currentPos = _PathGridController.FinnishPosition;
                del = _PathGridController.SetFinnish;
                break;
                default: return;
            }

            /* getting the desired position */
            if(currentPos.HasValue)
            {
                switch(vector)
                {
                    case MovementVector.DOWN: desiredPos = new Vector2Int(currentPos.Value.x, currentPos.Value.y-1);
                    break;
                    case MovementVector.UP: desiredPos = new Vector2Int(currentPos.Value.x, currentPos.Value.y+1);
                    break;
                    case MovementVector.LEFT: desiredPos = new Vector2Int(currentPos.Value.x-1, currentPos.Value.y);
                    break;
                    case MovementVector.RIGHT: desiredPos = new Vector2Int(currentPos.Value.x+1, currentPos.Value.y);
                    break;
                }

            }
            else return;

            /* screen controller */
            _ScreenController.ChangeArrowLabel(vector);

            /* executing the right function */
            del(desiredPos);
        }

    }
}
