// GENERATED AUTOMATICALLY FROM 'Assets/Demo/Input/Input.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @Input : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @Input()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Input"",
    ""maps"": [
        {
            ""name"": ""Movement"",
            ""id"": ""808b34a8-56c0-4ff6-8904-69eab28d5d87"",
            ""actions"": [
                {
                    ""name"": ""MoveUp"",
                    ""type"": ""Button"",
                    ""id"": ""f4857996-81b6-4a92-bfad-ae6d4ce898e2"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MoveDown"",
                    ""type"": ""Button"",
                    ""id"": ""e029faf9-674f-41a1-8108-e6cd8239c436"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MoveLeft"",
                    ""type"": ""Button"",
                    ""id"": ""5562512e-ebb9-4a92-9175-3bf378b8c0a1"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MoveRight"",
                    ""type"": ""Button"",
                    ""id"": ""522b3a28-0fe3-4310-8f02-cb4a7cf626dd"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""327368fa-46a1-42f7-a96b-2553baa81293"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveUp"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""10bbf82c-7d5f-47fa-9f9a-333913e52dbc"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveDown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""66a1b0d9-d602-4728-9d40-b27cd892e268"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7bb6aa78-49be-49a8-abab-7d1fb9f5c0db"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""MoveableCell"",
            ""id"": ""c033ccea-ff5d-4936-a36b-fa669f72711f"",
            ""actions"": [
                {
                    ""name"": ""SetMoveableCell"",
                    ""type"": ""Button"",
                    ""id"": ""52d4203d-1856-4929-918e-39f8499fb050"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""350f168b-5a59-4b08-9d0b-7b056776f0f7"",
                    ""path"": ""<Keyboard>/tab"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SetMoveableCell"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""MouseCellSelection"",
            ""id"": ""6771e5e9-3cae-48db-a1f0-191c0c3b0b56"",
            ""actions"": [
                {
                    ""name"": ""SelectCell"",
                    ""type"": ""Button"",
                    ""id"": ""28f14d51-d0d4-4707-bcaa-a8439e252f7b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""630b6829-1973-4de8-a1d7-103984e80857"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SelectCell"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Movement
        m_Movement = asset.FindActionMap("Movement", throwIfNotFound: true);
        m_Movement_MoveUp = m_Movement.FindAction("MoveUp", throwIfNotFound: true);
        m_Movement_MoveDown = m_Movement.FindAction("MoveDown", throwIfNotFound: true);
        m_Movement_MoveLeft = m_Movement.FindAction("MoveLeft", throwIfNotFound: true);
        m_Movement_MoveRight = m_Movement.FindAction("MoveRight", throwIfNotFound: true);
        // MoveableCell
        m_MoveableCell = asset.FindActionMap("MoveableCell", throwIfNotFound: true);
        m_MoveableCell_SetMoveableCell = m_MoveableCell.FindAction("SetMoveableCell", throwIfNotFound: true);
        // MouseCellSelection
        m_MouseCellSelection = asset.FindActionMap("MouseCellSelection", throwIfNotFound: true);
        m_MouseCellSelection_SelectCell = m_MouseCellSelection.FindAction("SelectCell", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Movement
    private readonly InputActionMap m_Movement;
    private IMovementActions m_MovementActionsCallbackInterface;
    private readonly InputAction m_Movement_MoveUp;
    private readonly InputAction m_Movement_MoveDown;
    private readonly InputAction m_Movement_MoveLeft;
    private readonly InputAction m_Movement_MoveRight;
    public struct MovementActions
    {
        private @Input m_Wrapper;
        public MovementActions(@Input wrapper) { m_Wrapper = wrapper; }
        public InputAction @MoveUp => m_Wrapper.m_Movement_MoveUp;
        public InputAction @MoveDown => m_Wrapper.m_Movement_MoveDown;
        public InputAction @MoveLeft => m_Wrapper.m_Movement_MoveLeft;
        public InputAction @MoveRight => m_Wrapper.m_Movement_MoveRight;
        public InputActionMap Get() { return m_Wrapper.m_Movement; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MovementActions set) { return set.Get(); }
        public void SetCallbacks(IMovementActions instance)
        {
            if (m_Wrapper.m_MovementActionsCallbackInterface != null)
            {
                @MoveUp.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveUp;
                @MoveUp.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveUp;
                @MoveUp.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveUp;
                @MoveDown.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveDown;
                @MoveDown.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveDown;
                @MoveDown.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveDown;
                @MoveLeft.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveLeft;
                @MoveLeft.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveLeft;
                @MoveLeft.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveLeft;
                @MoveRight.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveRight;
                @MoveRight.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveRight;
                @MoveRight.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveRight;
            }
            m_Wrapper.m_MovementActionsCallbackInterface = instance;
            if (instance != null)
            {
                @MoveUp.started += instance.OnMoveUp;
                @MoveUp.performed += instance.OnMoveUp;
                @MoveUp.canceled += instance.OnMoveUp;
                @MoveDown.started += instance.OnMoveDown;
                @MoveDown.performed += instance.OnMoveDown;
                @MoveDown.canceled += instance.OnMoveDown;
                @MoveLeft.started += instance.OnMoveLeft;
                @MoveLeft.performed += instance.OnMoveLeft;
                @MoveLeft.canceled += instance.OnMoveLeft;
                @MoveRight.started += instance.OnMoveRight;
                @MoveRight.performed += instance.OnMoveRight;
                @MoveRight.canceled += instance.OnMoveRight;
            }
        }
    }
    public MovementActions @Movement => new MovementActions(this);

    // MoveableCell
    private readonly InputActionMap m_MoveableCell;
    private IMoveableCellActions m_MoveableCellActionsCallbackInterface;
    private readonly InputAction m_MoveableCell_SetMoveableCell;
    public struct MoveableCellActions
    {
        private @Input m_Wrapper;
        public MoveableCellActions(@Input wrapper) { m_Wrapper = wrapper; }
        public InputAction @SetMoveableCell => m_Wrapper.m_MoveableCell_SetMoveableCell;
        public InputActionMap Get() { return m_Wrapper.m_MoveableCell; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MoveableCellActions set) { return set.Get(); }
        public void SetCallbacks(IMoveableCellActions instance)
        {
            if (m_Wrapper.m_MoveableCellActionsCallbackInterface != null)
            {
                @SetMoveableCell.started -= m_Wrapper.m_MoveableCellActionsCallbackInterface.OnSetMoveableCell;
                @SetMoveableCell.performed -= m_Wrapper.m_MoveableCellActionsCallbackInterface.OnSetMoveableCell;
                @SetMoveableCell.canceled -= m_Wrapper.m_MoveableCellActionsCallbackInterface.OnSetMoveableCell;
            }
            m_Wrapper.m_MoveableCellActionsCallbackInterface = instance;
            if (instance != null)
            {
                @SetMoveableCell.started += instance.OnSetMoveableCell;
                @SetMoveableCell.performed += instance.OnSetMoveableCell;
                @SetMoveableCell.canceled += instance.OnSetMoveableCell;
            }
        }
    }
    public MoveableCellActions @MoveableCell => new MoveableCellActions(this);

    // MouseCellSelection
    private readonly InputActionMap m_MouseCellSelection;
    private IMouseCellSelectionActions m_MouseCellSelectionActionsCallbackInterface;
    private readonly InputAction m_MouseCellSelection_SelectCell;
    public struct MouseCellSelectionActions
    {
        private @Input m_Wrapper;
        public MouseCellSelectionActions(@Input wrapper) { m_Wrapper = wrapper; }
        public InputAction @SelectCell => m_Wrapper.m_MouseCellSelection_SelectCell;
        public InputActionMap Get() { return m_Wrapper.m_MouseCellSelection; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MouseCellSelectionActions set) { return set.Get(); }
        public void SetCallbacks(IMouseCellSelectionActions instance)
        {
            if (m_Wrapper.m_MouseCellSelectionActionsCallbackInterface != null)
            {
                @SelectCell.started -= m_Wrapper.m_MouseCellSelectionActionsCallbackInterface.OnSelectCell;
                @SelectCell.performed -= m_Wrapper.m_MouseCellSelectionActionsCallbackInterface.OnSelectCell;
                @SelectCell.canceled -= m_Wrapper.m_MouseCellSelectionActionsCallbackInterface.OnSelectCell;
            }
            m_Wrapper.m_MouseCellSelectionActionsCallbackInterface = instance;
            if (instance != null)
            {
                @SelectCell.started += instance.OnSelectCell;
                @SelectCell.performed += instance.OnSelectCell;
                @SelectCell.canceled += instance.OnSelectCell;
            }
        }
    }
    public MouseCellSelectionActions @MouseCellSelection => new MouseCellSelectionActions(this);
    public interface IMovementActions
    {
        void OnMoveUp(InputAction.CallbackContext context);
        void OnMoveDown(InputAction.CallbackContext context);
        void OnMoveLeft(InputAction.CallbackContext context);
        void OnMoveRight(InputAction.CallbackContext context);
    }
    public interface IMoveableCellActions
    {
        void OnSetMoveableCell(InputAction.CallbackContext context);
    }
    public interface IMouseCellSelectionActions
    {
        void OnSelectCell(InputAction.CallbackContext context);
    }
}
