﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;
using UnityEngine.InputSystem;


public delegate void VectorDelegate(MovementVector vector);
public class InputListener : MonoBehaviour
{
    public event VectorDelegate                 movementChanged;
    public event moveableCellDelegate           moveableCellChanged;

    public CellState                            MoveableCell {get; private set;}
    private Input                               _Input = null;
    private List<MovementVector>                _VectorStack = null;

    /* MONOBEHAVIOUR */
    private void Awake() {  _Input = new Input();   }
    private void OnEnable() {   _Input.Enable();    }
    private void OnDisable() {  _Input.Disable();   }

    private void Start() 
    {
        /* VECTORS */
        _VectorStack = new List<MovementVector>();
        _VectorStack.Add(MovementVector.NONE);
        /* MOVEMENT */
        _Input.Movement.MoveDown.started += _ => AddToVectorStack(MovementVector.DOWN);
        _Input.Movement.MoveUp.started +=  _ => AddToVectorStack(MovementVector.UP);
        _Input.Movement.MoveLeft.started +=  _ => AddToVectorStack(MovementVector.LEFT);
        _Input.Movement.MoveRight.started +=  _ => AddToVectorStack(MovementVector.RIGHT);
        _Input.Movement.MoveDown.performed +=  _ => RemoveFromVectorStack(MovementVector.DOWN);
        _Input.Movement.MoveUp.performed +=  _ => RemoveFromVectorStack(MovementVector.UP);
        _Input.Movement.MoveLeft.performed +=  _ => RemoveFromVectorStack(MovementVector.LEFT);
        _Input.Movement.MoveRight.performed +=  _ => RemoveFromVectorStack(MovementVector.RIGHT);
        /* START & FINNISH */
        MoveableCell = CellState.START;
        _Input.MoveableCell.SetMoveableCell.started += _ => SwitchMoveableCell();
    }

    /* VECTOR STACK */
    // kazde pridani a oddelani vektoru odesle informaci o tom, jaky vektor je posledni zbyvajici
    private void AddToVectorStack(MovementVector vector)
    {
        _VectorStack.Add(vector);
        movementChanged?.Invoke(_VectorStack.Last());
    }
    private void RemoveFromVectorStack(MovementVector vector)
    {
        _VectorStack.Remove(vector);
        movementChanged?.Invoke(_VectorStack.Last());
    }  

    /* MOVEABLE CELLS */
    private void SwitchMoveableCell()
    {
        if(MoveableCell == CellState.START) MoveableCell = CellState.FINNISH;
        else if(MoveableCell == CellState.FINNISH) MoveableCell = CellState.START;
        else throw new NotImplementedException();

        moveableCellChanged?.Invoke(MoveableCell);
    }
}
