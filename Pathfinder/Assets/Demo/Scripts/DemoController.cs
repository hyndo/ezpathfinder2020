﻿using UnityEngine;

public class DemoController : MonoBehaviour
{
    [Header("Ground")]
    [SerializeField] private Transform                  _Origin = null;
    [Header("Prefabs")]
    [SerializeField] private GameObject                 _CubePrefab = null;
    [Header("Scripts")]
    [SerializeField] private GridController             _GridController = null;

    private float                                       _GroundOffset = 0;
    private float                                       _CubeWidth = 0;
    private GroundCubeController[,]                     _CellCubes = null;
    
    /* MONOBEHAVIOUR */
    private void Awake() 
    {
        _GridController.cellsSet += OnCellsSet;
        _GridController.startSet += OnStartSet;
        _GridController.neutralSet += OnNeutralSet;
        _GridController.finnishSet += OnFinnishSet;
        _GridController.pathSet += OnPathSet;
        _GridController.obstacleSet += OnObstacleSet;

        Mesh mesh = _CubePrefab.GetComponentInChildren<MeshFilter>().sharedMesh;
       
        _CubeWidth = mesh.bounds.size.x;
        _GroundOffset = _CubeWidth/2;
    }

    /* PATH GRID SUBSCRIPTION */
    private void OnCellsSet()
    {
        Vector2Int groundSize = new Vector2Int(_GridController.Width, _GridController.Height);
        _CellCubes = new GroundCubeController[groundSize.x,groundSize.y];
        GenerateCells(groundSize);
    }
    private void OnStartSet(Vector2Int? position)
    {
        if(position.HasValue) _CellCubes[position.Value.x,position.Value.y].SetStart();
    }
    private void OnFinnishSet(Vector2Int? position)
    {
        if(position.HasValue) _CellCubes[position.Value.x,position.Value.y].SetFinnish();
    }
    private void OnNeutralSet(Vector2Int? position)
    {
        if(position.HasValue) _CellCubes[position.Value.x, position.Value.y].SetNeutral();
        
    }

    private void OnObstacleSet(Vector2Int? position)
    {
        if(position.HasValue) _CellCubes[position.Value.x, position.Value.y].SetObstacle();
    }

    /* PATHFINDER SUBSCRIPTION */
    private void OnPathSet(Vector2Int? position)
    {
        if(position.HasValue) _CellCubes[position.Value.x, position.Value.y].SetPath(); 
    }

    /* GRID GENERATION */
    private void GenerateCells(Vector2Int groundSize)
    {
        for(int i=0; i< groundSize.x; i++)
        {
            for(int j=0; j< groundSize.y; j++)
            {
                GameObject go = Instantiate(_CubePrefab, GetCellPosition(i,j), Quaternion.identity);
                _CellCubes[i,j] = go.GetComponent<GroundCubeController>();
                bool currentCheckering = (i + j) % 2 == 0;
                _CellCubes[i,j].SetNeutral(currentCheckering);
            }
        }
    }

    /* POSITION GENERATION */
    private Vector3 GetCellPosition(int width, int height)
    {
        Vector3 originPosition = _Origin.position;
        return new Vector3
        (
            originPosition.x + width*_CubeWidth + _GroundOffset, 
            _GroundOffset, 
            originPosition.z + height*_CubeWidth +_GroundOffset
        );
    }
}
