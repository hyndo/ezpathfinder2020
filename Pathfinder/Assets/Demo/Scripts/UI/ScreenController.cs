﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections;

public class ScreenController : MonoBehaviour
{
    [Header("Data")]
    [SerializeField] private DemoColors _DemoColors = null;
    [Header("UI - Moveable cell")]
    [SerializeField] private Image _MoveableCellLabel = null;
    [SerializeField] private TextMeshProUGUI _MoveableCellLabelText = null;
    [SerializeField] private Image _MoveableCellControl = null;
    [Header("UI - Arrows")]
    [SerializeField] private Image[] _ArrowLabels = null;
    [SerializeField] private Image[] _ArrowControls = null;

    private int                 _LastArrowIndex = -1;
    void Awake()
    {
        _MoveableCellLabel.color = GetTransparentColor(_DemoColors.startColor, 0.4f);
        _MoveableCellControl.color = _DemoColors.controlColor;

        for(int i=0; i< _ArrowControls.Length; i++)
        {
            MovementVector currentVector = (MovementVector)(i+1);
            _ArrowControls[i].color = _DemoColors.controlColor;
            _ArrowLabels[i].color = _DemoColors.controlLabelColor;
        }
    }

    public void ChangeMoveableCellLabel(CellState state)
    {
        switch(state)
        {
            case CellState.START:
            _MoveableCellLabel.color = GetTransparentColor(_DemoColors.startColor, 0.4f);
            _MoveableCellLabelText.text = "START";
            break;
            case CellState.FINNISH:
            _MoveableCellLabel.color = GetTransparentColor(_DemoColors.finnishColor, 0.4f);
            _MoveableCellLabelText.text = "FINNISH";
            break;
            default: throw new NotImplementedException();
        }
    }

    public void ChangeArrowLabel(MovementVector vector)
    {
        try
        {
            int currentArrowIndex = (int)vector-1;
            if(currentArrowIndex != _LastArrowIndex)
            {
                StartCoroutine(FadeActivationCor(_ArrowLabels[currentArrowIndex]));
                _LastArrowIndex = currentArrowIndex;
            }
                
        }
        catch(Exception e)
        {
            Debug.LogError($"Arrow label at index {(int)vector-1} could not be accessed! << {e.Message}");
        }
    }

    private Color GetTransparentColor(Color color, float transparency)
    {
        return new Color(color.r,color.g,color.b, Mathf.Clamp01(transparency));
    }

    private IEnumerator FadeActivationCor(Image label)
    {
        float duration = 0.75f;
        float phase = 0f;
        float elapsed = 0f;
        Color activatedColor = _DemoColors.controlLabelActivationColor;
        Color normalColor = _DemoColors.controlLabelColor;
        while(phase < 1)
        {
            Color currentColor = Color.Lerp(activatedColor,normalColor,phase);
            label.color = currentColor;
            elapsed += Time.deltaTime;
            phase = Mathf.Clamp01(elapsed / duration);
            yield return null;
        }
        _LastArrowIndex = -1;
    }
}
