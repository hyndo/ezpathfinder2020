﻿using UnityEngine;

[CreateAssetMenu(fileName = "Colors#", menuName = "PathfinderDemo/Colors")]
public class DemoColors : ScriptableObject
{
    /* CELLS */
    public Color               startColor;
    public Color               finnishColor;
    public Color               neutralColorLight;
    public Color               neutralColorDark;
    public Color               pathColor;
    public Color               obstacleColor;

    /* UI */
    public Color               controlColor;
    public Color               controlLabelColor;
    public Color               controlLabelActivationColor;
}
