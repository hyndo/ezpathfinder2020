﻿using UnityEngine;

public class GroundCubeController : MonoBehaviour
{
    [SerializeField] private DemoColors             _DemoColors = null;
    [SerializeField] private Renderer               _Renderer = null;
    private bool?                                   _LocalCheckering = null;

    public void SetNeutral(bool checkering = false)
    {
        if(_LocalCheckering == null) _LocalCheckering = checkering;
        if(_LocalCheckering.Value) _Renderer.material.color = _DemoColors.neutralColorLight;
        else if(!_LocalCheckering.Value) _Renderer.material.color = _DemoColors.neutralColorDark;
    }

    public void SetStart()
    {
        _Renderer.material.color = _DemoColors.startColor; 
    }

    public void SetFinnish()
    {
        _Renderer.material.color = _DemoColors.finnishColor; 
    }

    public void SetPath()
    {
        _Renderer.material.color = _DemoColors.pathColor;
    }

    public void SetObstacle()
    {
        _Renderer.material.color = _DemoColors.obstacleColor;
    }
}
