﻿using UnityEngine;

/// <summary>
/// A cell in a pathfinding grid
/// </summary>
public class GridCell: IHeapItem<GridCell>
{
    /// <summary>
    /// Creates a gridcell on a given position
    /// </summary>
    /// <param name="x">Horizontal position</param>
    /// <param name="y">Vertical position</param>
    public GridCell(int x, int y)
    {
        Parent = null;
        Position = new Vector2Int(x,y);
        State = CellState.NEUTRAL;
        GCost = 0;
        HCost = 0;
    }

    /* PATHFINDING */
    /// <summary>
    /// Previous gridcell in a path
    /// </summary>
    public GridCell                 Parent { get; set; }

    /// <summary>
    /// Horizontal position
    /// </summary>
    public int                      X => Position.x;

    /// <summary>
    /// Vertical position
    /// </summary>
    public int                      Y => Position.y;

    /// <summary>
    /// Position 
    /// </summary>
    public Vector2Int               Position { get; private set; } 

    /// <summary>
    /// Current state of the gridcell, eg. Start, Finnish, Obstacle etc.
    /// </summary>
    public CellState                State{ get; set; }

    /// <summary>
    /// A cost of getting from the start to this gridcell
    /// </summary>
    public int                      GCost { get; set; }

    /// <summary>
    /// A cost of getting from this gridcell to the finnish
    /// </summary>
    public int                      HCost { get; set; }

    /// <summary>
    /// Overall cost of GCost + HCost
    /// </summary>
    public int                      FCost => GCost + HCost;

    /* HEAP OPTIMIZATION */
    /// <summary>
    /// Index inside a heap of gridcells
    /// </summary>
    public int                      HeapIndex { get; set; }

    /// <summary>
    /// Compares pathfinding cost of this cell to the pathfinding cost of another cell 
    /// </summary>
    /// <param name="cell">A cell to campare this cell to</param>
    /// <returns>Comparison result between -1 and 1</returns>
    public int CompareTo(GridCell cell)
    {
        int compare  = FCost.CompareTo(cell.FCost);
        if(compare == 0)
        {
            compare = HCost.CompareTo(cell.HCost);
        }
        return -compare;
    }
}
