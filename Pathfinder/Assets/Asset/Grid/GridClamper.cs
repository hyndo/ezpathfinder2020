﻿using UnityEngine;

/// <summary>
/// Clamps positional value inside a pathfinding grid
/// </summary>
public class GridClamper 
{
    public GridClamper(int width, int height)
    {
        _Width = width;
        _Height = height;
    }

    private int _Width = 0;
    private int _Height = 0; 
    

    /* CONSTRAINTS */
    /// <summary>
    /// Constrains coordinate inside a pathfinding grid
    /// </summary>
    public Vector2Int? ConstraintCoordinate(Vector2Int? coordinate)
    {
        if(coordinate != null) return new Vector2Int?(new Vector2Int(Mathf.Clamp(coordinate.Value.x, 0,_Width-1), Mathf.Clamp(coordinate.Value.y,0,_Height-1)));
        else return null; 
    }

    /// <summary>
    /// Constrains coordinate inside a pathfinding grid
    /// </summary>
    public Vector2Int ConstraintCoordinate(Vector2Int coordinate)
    {
        return new Vector2Int(Mathf.Clamp(coordinate.x, 0,_Width-1), Mathf.Clamp(coordinate.y,0,_Height-1)); 
    }

    /// <summary>
    /// Constrains width bounds inside a pathfinding grid
    /// </summary>
    public Vector2Int ConstraintWidthBounds(Vector2Int bounds)
    {
        return new Vector2Int(Mathf.Clamp(bounds.x, 0, _Width-1), Mathf.Clamp(bounds.y, 0, _Width-1));
    }

    /// <summary>
    /// Constrains height bounds inside a pathfinding grid
    /// </summary>
    public Vector2Int ConstraintHeightBounds(Vector2Int bounds)
    {
        return new Vector2Int(Mathf.Clamp(bounds.x, 0, _Height-1), Mathf.Clamp(bounds.y, 0, _Height-1));
    }
}
