﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Creates and maintains a pathfinding grid
/// </summary>
public class GridController : MonoBehaviour
{
    /* EVENTS */
    /// <summary>
    /// Fires when the pathfinding grid is set 
    /// </summary>
    public event muteDelegate                               cellsSet;

    /// <summary>
    /// Fires when start of a path is set
    /// </summary>
    public event positionalDelegate                         startSet;

    /// <summary>
    /// Fires when a finnish of a path is set
    /// </summary>
    public event positionalDelegate                         finnishSet;

    /// <summary>
    /// Fires when a neutral gridcell is set
    /// </summary>
    public event positionalDelegate                         neutralSet;

    /// <summary>
    /// Fires when the whole path is set
    /// </summary>
    public event positionalDelegate                         pathSet;

    /// <summary>
    /// Fires when an obstacle gridcell is set
    /// </summary>
    public event positionalDelegate                         obstacleSet;

    /* GRID */
    /// <summary>
    /// The number of all gridcells in the pathfinding grid
    /// </summary>
    public int                                              TotalGridcellAmount => Width * Height; 

    /// <summary>
    /// Width of the pathfinding grid
    /// </summary>
    public int                                              Width => _Width;

    /// <summary>
    /// Height of the pathfinding grid
    /// </summary>
    public int                                              Height => _Height;

    /// <summary>
    /// Gridcells in the pathfinding grid
    /// </summary>
    public GridCell[,]                                      Cells { get; private set; }

    /// <summary>
    /// Starting position of a path
    /// </summary>
    public Vector2Int?                                      StartPosition { get; private set; }

    /// <summary>
    /// Finnisshing position of a path
    /// </summary>
    public Vector2Int?                                      FinnishPosition { get; private set; }

    /* EDITOR */
    [Header("Dimensions")]
    [SerializeField] private int                            _Width = 30;
    [SerializeField] private int                            _Height = 20;
    [Header("Defaults")]
    [SerializeField] private Vector2Int                     _DefaultStart = Vector2Int.zero;
    [SerializeField] private Vector2Int                     _DefaultFinnish = Vector2Int.one;
    [Header("Scripts")]
    [SerializeField] private PathFinder                     _PathFinder = null;
    [SerializeField] private ObstacleGenerator              _ObstacleGenerator = null;

    private GridClamper                                     _GridClamper = null;

    /* PATH */
    private List<Vector2Int>                                _CurrentPath = new List<Vector2Int>();
    
    /* MONOBEHAVIOUR */
    private void Awake() 
    {
        _ObstacleGenerator.obstacleGenerated += OnObstacleGenerated;
        _GridClamper = new GridClamper(_Width, _Height);
    }

    private void Start() 
    {
        /* cells */
        Cells = new GridCell[_Width, _Height];
        for(int i=0; i< _Width; i++)
        {
            for(int j=0; j< _Height; j++)
            {
                Cells[i,j] = new GridCell(i,j);
            }
        }
        cellsSet?.Invoke();

        /* start */
        StartPosition = _GridClamper.ConstraintCoordinate(_DefaultStart);
        startSet?.Invoke(StartPosition);

        /* finnish */
        FinnishPosition = _GridClamper.ConstraintCoordinate(_DefaultFinnish);
        finnishSet?.Invoke(FinnishPosition);

        /* obstacle generation */
        _ObstacleGenerator.GenerateEditorObstacles();

        /* initial path */
        ProcessPath(_PathFinder.GetPath(StartPosition,FinnishPosition));
    }

    /* START & FINNISH */
    /// <summary>
    /// Sets start of a path
    /// </summary>
    /// <param name="start">Start position</param>
    public void SetStart(Vector2Int? start)
    {
        start = _GridClamper.ConstraintCoordinate(start);
        if(start != FinnishPosition && StartOrFinnishCandidate(start.Value))
        {
            if(StartPosition != null)
            {
                Cells[StartPosition.Value.x, StartPosition.Value.y].State = CellState.NEUTRAL;
                neutralSet?.Invoke(StartPosition);
            }
            StartPosition = start;
            Cells[StartPosition.Value.x, StartPosition.Value.y].State = CellState.START;
            startSet?.Invoke(StartPosition);

            ProcessPath(_PathFinder.GetPath(StartPosition,FinnishPosition));
        }
    }

    /// <summary>
    /// Sets finnish of a path
    /// </summary>
    /// <param name="finnish">Finnish position</param>
    public void SetFinnish(Vector2Int? finnish)
    {
        finnish = _GridClamper.ConstraintCoordinate(finnish);
        if(finnish != StartPosition && StartOrFinnishCandidate(finnish.Value))
        {
            if(FinnishPosition != null) 
            {
                Cells[FinnishPosition.Value.x, FinnishPosition.Value.y].State = CellState.NEUTRAL;
                neutralSet?.Invoke(FinnishPosition);
            }
            FinnishPosition = finnish;
            Cells[FinnishPosition.Value.x, FinnishPosition.Value.y].State = CellState.FINNISH;
            finnishSet?.Invoke(FinnishPosition);

            ProcessPath(_PathFinder.GetPath(StartPosition,FinnishPosition));
        }
    }

    private bool StartOrFinnishCandidate(Vector2Int position)
    {
        GridCell cell = Cells[position.x,position.y];
        return cell.State != CellState.OBSTACLE;
    }
    
    /* OBSTACLES */
    private void OnObstacleGenerated(List<Vector2Int> obstacle)
    {
        if(obstacle != null)
        {
            for(int i=0; i< obstacle.Count; i++)
            {
                Vector2Int position = _GridClamper.ConstraintCoordinate(obstacle[i]);
                Cells[position.x,position.y].State = CellState.OBSTACLE;
                obstacleSet?.Invoke(new Vector2Int?(position));
            }
        }
    }
    
    /* PATH */
    private void ProcessPath(List<Vector2Int> path)
    {
        if(_CurrentPath != null)
        {
            for(int p=0; p< _CurrentPath.Count; p++)
            {
                Vector2Int position = _CurrentPath[p];
                if(position != StartPosition && position != FinnishPosition)
                {
                    Cells[position.x,position.y].State = CellState.NEUTRAL;
                    neutralSet?.Invoke(new Vector2Int?(position));
                }
            }
             _CurrentPath.Clear();
        }

        if(path != null)
        {
            for(int i=0; i< path.Count; i++)
            {
                Vector2Int position = _GridClamper.ConstraintCoordinate(path[i]);
                _CurrentPath.Add(position);
                Cells[position.x, position.y].State = CellState.PATH;
                pathSet?.Invoke(new Vector2Int?(position));
            }
        }
    }

    /* BORDERING GRIDCELLS */
    public List<GridCell> GetBordering(Vector2Int position)
    {
        List<GridCell> results = new List<GridCell>();
        Vector2Int xBounds = _GridClamper.ConstraintWidthBounds(new Vector2Int(position.x-1, position.x+1));
        Vector2Int yBounds = _GridClamper.ConstraintHeightBounds(new Vector2Int(position.y-1, position.y+1));

        for(int x=xBounds.x; x<=xBounds.y; x++)
        {
            for(int y=yBounds.x; y<=yBounds.y; y++)
            {
                GridCell cell = Cells[x,y];
                if(cell.Position != position && !results.Contains(cell))
                {
                    results.Add(cell);
                }
            }
        }

        if(results.Count == 0) Debug.LogWarning("No bordering gridcells were found!");
        return results;
    }
}
