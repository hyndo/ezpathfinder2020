﻿using System;
using UnityEngine;

/// <summary>
/// An obstacle in a pathfinding grid
/// </summary>
[Serializable]
public struct Obstacle
{
    /// <summary>
    /// Type of the obstacle to be generated
    /// </summary>
    public ObstacleType     Type;

    /// <summary>
    /// Vectors for generating the obstacle
    /// </summary>
    public Vector2Int[]     Vectors;
}
