﻿using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Generates obstacles in the pathfinding grid
/// </summary>
public class ObstacleGenerator : MonoBehaviour
{
    /* EVENTS */
    /// <summary>
    /// Fires when creating an obstacle
    /// </summary>
    public event pathDelegate                           obstacleGenerated;

    /* EDITOR */
    [Header("Obstacles")]
    [Tooltip(PathfinderConstants.OBSTACLE_VECTORS_TOOLTIP)]
    [SerializeField] private List<Obstacle>             _Obstacles = null;

    /* OBSTACLE VALIDATION */
    private bool ValidPoint(Obstacle obstacle)
    {
        if(obstacle.Vectors == null) return false;
        else return obstacle.Vectors.Length == 1;
    }
    private bool ValidPoints(Obstacle obstacle)
    {
        if(obstacle.Vectors == null) return false;
        else return obstacle.Vectors.Length > 0;
    }
    private bool ValidWall(Obstacle obstacle)
    {
        if(obstacle.Vectors == null || obstacle.Vectors.Length != 2) return false;
        else
        {
            Vector2Int start = obstacle.Vectors[0], end = obstacle.Vectors[1];
            return (start.x != end.x && start.y == end.y) || 
            (start.x == end.x && start.y != end.y);
        }
    }
    private bool ValidRectangle(Obstacle obstacle)
    {
        if(obstacle.Vectors == null || obstacle.Vectors.Length != 2) return false;
        else
        {
            Vector2Int end = obstacle.Vectors[1];
            return (end.x > 1 && end.y > 1);
        }
    }
    
    /* OBSTACLE GENERATION */
    /// <summary>
    /// Generates obstacles set in editor
    /// </summary>
    public void GenerateEditorObstacles()
    {
        GenerateObstacles(_Obstacles);
    } 
    
    /// <summary>
    /// Generate a single coordinate obstacle
    /// </summary>
    /// <param name="obstacle">Obstacle to be generated</param>
    public List<Vector2Int> GeneratePoint(Obstacle obstacle)
    {
        List<Vector2Int> result = new List<Vector2Int>();
        result.Add(obstacle.Vectors[0]);
        return result;
    }
    
    /// <summary>
    /// Generates multiple obstacles on different coordinates
    /// </summary>
    /// <param name="obstacle">Obstacle to be generated</param>
    public List<Vector2Int> GeneratePoints(Obstacle obstacle)
    {
        List<Vector2Int> result = new List<Vector2Int>();
        for(int i=0; i< obstacle.Vectors.Length; i++)
        {
            if(!result.Contains(obstacle.Vectors[i]))
                result.Add(obstacle.Vectors[i]);
        }
        return result;
    }
    
    /// <summary>
    /// Generates a straight wall obstacle
    /// </summary>
    /// <param name="obstacle">Obstacle to be generated</param>
    /// <param name="checkered">States whether all even bricks should stay empty?</param>
    public List<Vector2Int> GenerateWall(Obstacle obstacle, bool checkered)
    {
        List<Vector2Int> result = new List<Vector2Int>();
        Vector2Int oldStart = obstacle.Vectors[0], oldEnd = obstacle.Vectors[1];
        Vector2Int newStart = oldStart, newEnd = oldEnd;
        bool horizontal = oldStart.x != oldEnd.x;
        
        if(oldStart.x > oldEnd.x || oldStart.y > oldEnd.y) 
        {
            newEnd = oldStart;
            newStart = oldEnd;
        }
        
        if(horizontal)
        {
            for(int x=newStart.x; x <= newEnd.x; x++)
               if(!checkered || x%2 == 0) result.Add(new Vector2Int(x,newStart.y));
        }
        else
        {
            for(int y=newStart.y; y <= newEnd.y; y++)
                 if(!checkered || y%2 == 0) result.Add(new Vector2Int(newStart.x,y));
        }
        return result;
    }
    
    /// <summary>
    /// Generated a rectangle shaped obstacle
    /// </summary>
    /// <param name="obstacle">Obstacle to be generated</param>
    public List<Vector2Int> GenerateRectangle(Obstacle obstacle)
    {
        List<Vector2Int> result = new List<Vector2Int>();

        /* Rectangle position and dimensions */
        Vector2Int position = obstacle.Vectors[0];
        Vector2Int dimensions = obstacle.Vectors[1];

        /* determining oddness */
        int oddWidth = dimensions.x % 2;
        int oddHeight = dimensions.y % 2;
        
        /* determining loop dimensions */
        int xStart = position.x - dimensions.x/2, yStart = position.y - dimensions.y/2;    
        int xEnd = position.x + dimensions.x/2 + oddWidth, yEnd = position.y + dimensions.y/2 + oddHeight;

        /* getting values */
        for(int x=xStart; x<=xEnd; x++)
        {
            for(int y=yStart; y<=yEnd; y++)
            {
                result.Add(new Vector2Int(x,y));
            }
        }

        return result;
    }

    /// <summary>
    /// Generates obstacles from a given list of obstacles
    /// </summary>
    /// <param name="obstacles">Obstacles to be generated</param>
    public void GenerateObstacles(List<Obstacle> obstacles)
    {
        for(int i=0; i< obstacles.Count; i++)
        {
            Obstacle currentObstacle = obstacles[i];
            switch(currentObstacle.Type)
            {
                case ObstacleType.POINT:
                    if(ValidPoint(currentObstacle)) obstacleGenerated?.Invoke(GeneratePoint(currentObstacle));
                    else throw Exceptions.InvalidObstacleFormatException;
                break;
                case ObstacleType.POINTS:
                    if(ValidPoints(currentObstacle)) obstacleGenerated?.Invoke(GeneratePoints(currentObstacle));
                    else throw Exceptions.InvalidObstacleFormatException;
                break;
                case ObstacleType.WALL:
                    if(ValidWall(currentObstacle))obstacleGenerated?.Invoke(GenerateWall(currentObstacle, false));
                    else throw Exceptions.InvalidObstacleFormatException;
                break;
                case ObstacleType.CHECKERED_WALL:
                    if(ValidWall(currentObstacle))obstacleGenerated?.Invoke(GenerateWall(currentObstacle, true));
                    else throw Exceptions.InvalidObstacleFormatException;
                break;
                case ObstacleType.RECTANGLE:
                    if(ValidRectangle(currentObstacle)) obstacleGenerated?.Invoke(GenerateRectangle(currentObstacle));
                    else throw Exceptions.InvalidObstacleFormatException;
                break;
                default: throw new NotImplementedException($"{currentObstacle.Type} obstacle type is not implemented!");
            }
        }
    }
}
