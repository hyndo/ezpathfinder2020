﻿/// <summary>
/// Simple binary tree
/// </summary>
public class Heap<T> where T: IHeapItem<T>
{
    public Heap(int maxHeapSize)
    {
        _Items = new T[maxHeapSize];
        Count = 0;
    }

    /// <summary>
    /// Items count
    /// </summary>
    public int              Count {get; private set;}
    private T[]             _Items = null;

    /// <summary>
    /// Adds an item into the heap
    /// </summary>
    public void Add(T item)
    {
        item.HeapIndex = Count;
        _Items[Count] = item;
        SortUp(item);
        Count++;
    }

    /// <summary>
    /// Removes and returns first item
    /// </summary>
    public T RemoveFirst()
    {
        T firstItem = _Items[0];
        Count--;
        _Items[0] = _Items[Count];
        _Items[0].HeapIndex = 0;
        SortDown(_Items[0]);
        return firstItem;
    }

    /// <summary>
    /// Checks whether the heap contains a given item
    /// </summary>
    public bool Contains(T item)
    {
        return Equals(_Items[item.HeapIndex], item); 
    }

    private void SortDown(T item)
    {
        while(true)
        {
            int childIndexLeft = item.HeapIndex*2 +1;
            int childIndexRight = item.HeapIndex*2 +2;
            int swapIndex = 0;

            if(childIndexLeft < Count)
            {
                swapIndex = childIndexLeft;
                if(childIndexRight < Count)
                {
                    if(_Items[childIndexLeft].CompareTo(_Items[childIndexRight]) < 0)
                    {
                        swapIndex = childIndexRight;
                    }
                }

                if(item.CompareTo(_Items[swapIndex])<0)
                {
                    Swap(item, _Items[swapIndex]);
                }
                else return;
        
            }
            else return;
        }
    }

    private void SortUp(T item)
    {
        int parentIndex = (item.HeapIndex-1)/2;
        while(true)
        {
            T parentItem = _Items[parentIndex];
            if(item.CompareTo(parentItem) > 0)
            {
                Swap(item, parentItem);
            }
            else break;

            parentIndex = (item.HeapIndex-1)/2;
        }
    }

    private void Swap(T itemA, T itemB)
    {
        _Items[itemA.HeapIndex] = itemB;
        _Items[itemB.HeapIndex] = itemA;

        int itemAIndex = itemA.HeapIndex;
        itemA.HeapIndex = itemB.HeapIndex;
        itemB.HeapIndex = itemAIndex;

    }
} 
