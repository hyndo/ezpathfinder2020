﻿using System;

/// <summary>
/// Interface for items that can go into the heap
/// </summary>
/// <typeparam name="T">Item type</typeparam>
public interface IHeapItem<T> : IComparable<T>
{
    int HeapIndex {get; set;}
}
