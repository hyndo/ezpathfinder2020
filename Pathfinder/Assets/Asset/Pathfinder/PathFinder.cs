﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// Finds a path between a start and a finnish
/// </summary>
public class PathFinder : MonoBehaviour
{

    [Header("Scripts")]
    [SerializeField] private GridController                 _GridController = null; 

    /// <summary>
    /// Gets a path between a start and a finnish
    /// </summary>
    /// <param name="start">starting position</param>
    /// <param name="finnish">finnishing position</param>
    /// <returns></returns>
    public List<Vector2Int> GetPath(Vector2Int? start, Vector2Int? finnish) 
    {
        if((start == null || finnish == null) || (start.Value == finnish.Value)) return null;
        else
        {
            Vector2Int dimensions = new Vector2Int(_GridController.Width, _GridController.Height);
            GridCell[,] cells = _GridController.Cells;

            GridCell startCell = cells[start.Value.x, start.Value.y];
            GridCell finnishCell = cells[finnish.Value.x, finnish.Value.y];
            
            Heap<GridCell> openList = new Heap<GridCell>(_GridController.TotalGridcellAmount);
            List<GridCell> closedList = new List<GridCell>();
            openList.Add(startCell);
            while(openList.Count > 0)
            {
                GridCell currentCell = openList.RemoveFirst();
                closedList.Add(currentCell);

                if(currentCell.Position == finnish.Value) return RetracePath(startCell, finnishCell);

                List<GridCell> bordering = _GridController.GetBordering(currentCell.Position);
                for(int j=0; j< bordering.Count; j++)
                {
                    GridCell currentBordering = bordering[j];
                    if(!IsPathCandidate(currentBordering) || closedList.Contains(currentBordering)) continue;
                    else
                    {
                        int newMovementCostToBordering = currentCell.GCost + CellDistance(currentCell, currentBordering);
                        if(newMovementCostToBordering < currentBordering.GCost || !openList.Contains(currentBordering))
                        {
                            currentBordering.GCost = newMovementCostToBordering;
                            currentBordering.HCost = CellDistance(currentBordering, finnishCell);
                            currentBordering.Parent = currentCell;
                            if(!openList.Contains(currentBordering))
                            {
                                openList.Add(currentBordering);
                            }
                        }
                    }
                }
            }
            return null;
        }
    }

    private List<Vector2Int> RetracePath(GridCell start, GridCell finnish)
    {
        List<GridCell> path = new List<GridCell>();
        GridCell currentCell = finnish;
        while(currentCell != start)
        {
            if(currentCell != start && currentCell != finnish)
                path.Add(currentCell);
            currentCell = currentCell.Parent;
        }
        path.Reverse();
        return path.Select(x=>x.Position).ToList();
    }

    private bool IsPathCandidate(GridCell cell)
    {
        return cell.State != CellState.OBSTACLE;
    }

    private int CellDistance(GridCell startCell, GridCell endCell)
    {
        int horizontalDistance = Mathf.Abs(startCell.X - endCell.X);
        int verticalDistance = Mathf.Abs(startCell.Y - endCell.Y);
        if(horizontalDistance > verticalDistance) return 14*verticalDistance + 10*(horizontalDistance-verticalDistance);
        else return 14*horizontalDistance +10*(verticalDistance - horizontalDistance);
    }
}
