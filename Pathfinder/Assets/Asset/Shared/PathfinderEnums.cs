﻿/// <summary>
/// Possible states of a GridCell
/// </summary>
public enum CellState { NEUTRAL, OBSTACLE, START, FINNISH, PATH };

/// <summary>
/// Types of obstacle
/// </summary>
public enum ObstacleType { POINT, POINTS, WALL, CHECKERED_WALL, RECTANGLE };
