﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Delegate that takes no arguments and returns nothing
/// </summary>
public delegate void muteDelegate();

/// <summary>
/// Delegate that takes in a position and returns nothing
/// </summary>
public delegate void positionalDelegate(Vector2Int? position);

/// <summary>
/// Delegate that takes in a list of positions and returns nothing
/// </summary>
public delegate void pathDelegate(List<Vector2Int> path);
