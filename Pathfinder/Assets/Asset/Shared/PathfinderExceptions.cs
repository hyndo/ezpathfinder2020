﻿using System;

/// <summary>
/// Shared exceptions between scripts
/// </summary>
public class Exceptions
{
    static Exceptions()
    {
        InvalidObstacleFormatException = new Exception("Program encountered obstacle with unresolvable vector values!");
    }

    /// <summary>
    /// Exception for the case of invalid vectors inside an obstacle
    /// </summary>
    public static Exception InvalidObstacleFormatException{ get; private set; } 
}
