﻿/// <summary>
/// Shared constants between scripts
/// </summary>
public class PathfinderConstants
{
    /// <summary>
    /// Tooltip for Obstacles list in ObstacleGenerator
    /// </summary>
    public const string OBSTACLE_VECTORS_TOOLTIP = "[POINT]\nRequires a single vector that signifies the position of the point.\n" +
    "[POINTS]\nRequires one or more vectors that signify positions of the points.\n" +
    "[WALL]\nRequires 2 vectors that signify start and end of the wall." + 
    "These vectors must have identical x coordinates and nonidentical y coordinates or vice versa.\n"+
    "[CHECKERED_WALL]\nThe same as in the case of WALL applies.\n" +
    "[RECTANGLE]\nRequires 2 vectors. First vector signifies position of the center of the rectangle. Second vector defines width and height of the rectangle.\n"+
    "Both width and height must be higher than 1.";
}

